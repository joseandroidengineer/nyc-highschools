package com.jge.a20190221;

import com.jge.a20190221.Models.School;

public interface ListItemClickListener {
    void onListItemClick(School schoolItem);
}
