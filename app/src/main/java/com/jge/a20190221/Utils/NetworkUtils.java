package com.jge.a20190221.Utils;

import android.net.Uri;

public class NetworkUtils {
    private static final String DBN_KEY = "dbn";
    private static final String BASE_URL = "https://data.cityofnewyork.us/";
    private static final String BASE_NUM_PARAM = "s3k6-pzi2.json";
    private static final String SAT_NUM_PARAM = "f9bf-2cp4.json";
    private static final String RES_PATH = "resource/";

    public static Uri buildBaseUrl() {
        Uri builtUri = Uri.parse(BASE_URL).buildUpon()
                .appendEncodedPath(RES_PATH)
                .appendEncodedPath(BASE_NUM_PARAM)
                .build();
        return builtUri;
    }
    public static Uri buildSATUrl(String key) {
        Uri builtUri = Uri.parse(BASE_URL).buildUpon()
                .appendEncodedPath(RES_PATH)
                .appendEncodedPath(SAT_NUM_PARAM)
                .appendQueryParameter(DBN_KEY,key)
                .build();
        return builtUri;
    }
}
