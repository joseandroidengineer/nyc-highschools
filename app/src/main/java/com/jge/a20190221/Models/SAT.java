package com.jge.a20190221.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SAT implements Parcelable {
    @SerializedName("dbn")
    private String dbn;
    @SerializedName("sat_critical_reading_avg_score")
    private String satReadingScore;
    @SerializedName("sat_math_avg_score")
    private String satMathScore;
    @SerializedName("sat_writing_avg_score")
    private String satWritingScore;

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getSatReadingScore() {
        return satReadingScore;
    }

    public void setSatReadingScore(String satReadingScore) {
        this.satReadingScore = satReadingScore;
    }

    public String getSatMathScore() {
        return satMathScore;
    }

    public void setSatMathScore(String satMathScore) {
        this.satMathScore = satMathScore;
    }

    public String getSatWritingScore() {
        return satWritingScore;
    }

    public void setSatWritingScore(String satWritingScore) {
        this.satWritingScore = satWritingScore;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(dbn);
        parcel.writeString(satMathScore);
        parcel.writeString(satWritingScore);
        parcel.writeString(satReadingScore);
    }

    public static final Parcelable.Creator<SAT> CREATOR = new Parcelable.Creator<SAT>() {
        public SAT createFromParcel(Parcel in){
            return new SAT(in);
        }

        public SAT[] newArray(int size){
            return new SAT[size];
        }
    };

    private SAT(Parcel in){
        dbn = in.readString();
        satReadingScore = in.readString();
        satWritingScore = in.readString();
        satMathScore = in.readString();
    }
}
