package com.jge.a20190221.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class School implements Parcelable {

    @SerializedName("dbn")
    private String dbn;
    @SerializedName("extracurricular_activities")
    private String extracurricularActivities;
    @SerializedName("borough")
    private String borough;
    @SerializedName("bus")
    private String bus;
    @SerializedName("primary_address_line_1")
    private String primaryAdress;
    @SerializedName("overview_paragraph")
    private String overviewParagraph;
    @SerializedName("phone_number")
    private String phoneNumber;
    @SerializedName("school_name")
    private String schoolName;
    @SerializedName("school_email")
    private String schoolEmail;
    @SerializedName("school_sports")
    private String sports;
    @SerializedName("subway")
    private String subway;
    @SerializedName("total_students")
    private String totalStudents;
    @SerializedName("website")
    private String website;
    private String satScore;


    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getExtracurricularActivities() {
        return extracurricularActivities;
    }

    public void setExtracurricularActivities(String extracurricularActivities) {
        this.extracurricularActivities = extracurricularActivities;
    }

    public String getBorough() {
        return borough;
    }

    public void setBorough(String borough) {
        this.borough = borough;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public String getPrimaryAdress() {
        return primaryAdress;
    }

    public void setPrimaryAdress(String primaryAdress) {
        this.primaryAdress = primaryAdress;
    }

    public String getOverviewParagraph() {
        return overviewParagraph;
    }

    public void setOverviewParagraph(String overviewParagraph) {
        this.overviewParagraph = overviewParagraph;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public void setSchoolEmail(String schoolEmail) {
        this.schoolEmail = schoolEmail;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }

    public String getSubway() {
        return subway;
    }

    public void setSubway(String subway) {
        this.subway = subway;
    }

    public String getTotalStudents() {
        return totalStudents;
    }

    public void setTotalStudents(String totalStudents) {
        this.totalStudents = totalStudents;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getSatScore() {
        return satScore;
    }

    public void setSatScore(String satScore) {
        this.satScore = satScore;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(dbn);
        parcel.writeString(satScore);
        parcel.writeString(schoolEmail);
        parcel.writeString(schoolName);
        parcel.writeString(sports);
        parcel.writeString(subway);
        parcel.writeString(extracurricularActivities);
        parcel.writeString(borough);
        parcel.writeString(bus);
        parcel.writeString(phoneNumber);
        parcel.writeString(primaryAdress);
        parcel.writeString(overviewParagraph);
        parcel.writeString(website);
        parcel.writeString(totalStudents);
    }

    public static final Parcelable.Creator<School> CREATOR = new Parcelable.Creator<School>() {
        public School createFromParcel(Parcel in){
            return new School(in);
        }

        public School[] newArray(int size){
            return new School[size];
        }
    };

    private School(Parcel in){
        dbn = in.readString();
        satScore = in.readString();
        schoolEmail = in.readString();
        schoolName = in.readString();
        sports = in.readString();
        subway = in.readString();
        extracurricularActivities = in.readString();
        borough = in.readString();
        bus = in.readString();
        phoneNumber = in.readString();
        primaryAdress = in.readString();
        overviewParagraph = in.readString();
        website = in.readString();
        totalStudents = in.readString();
    }
}
