package com.jge.a20190221;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jge.a20190221.Adapters.SchoolAdapter;
import com.jge.a20190221.Models.School;
import com.jge.a20190221.Utils.NetworkUtils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements ListItemClickListener{

    private RecyclerView mRecyclerView;
    private RequestQueue requestQueue;
    private Gson gson;
    private SchoolAdapter mSchoolAdapter;
    private static ArrayList<School> schoolArrayList;
    private final static String SAVED_INSTANCE_KEY = "schoolsArrayList";
    private Context context;
    private ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pb = findViewById(R.id.pb);
        pb.setVisibility(View.VISIBLE);
        context = getBaseContext();
        mRecyclerView = findViewById(R.id.schools_rv);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setHasFixedSize(true);
        mSchoolAdapter = new SchoolAdapter(this);
        mRecyclerView.setAdapter(mSchoolAdapter);
        volleyRequest();
        mSchoolAdapter.notifyDataSetChanged();
    }

    private void volleyRequest() {
        requestQueue = Volley.newRequestQueue(context);
        String url = String.valueOf(NetworkUtils.buildBaseUrl());
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                gsonMap(response);
                Toast.makeText(context, "Data Retrieved", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "There is a volley Error: "+error, Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonArrayRequest);
    }

    private void gsonMap(JSONArray jsonArray){
        if(jsonArray == null){
            Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
        }else{
            GsonBuilder gsonBuilder = new GsonBuilder();
            gson = gsonBuilder.create();
            if(jsonArray.length() > 0){
                schoolArrayList = new ArrayList<>(Arrays.asList(gson.fromJson(jsonArray.toString(), School[].class)));
                pb.setVisibility(View.GONE);
                mSchoolAdapter.setSchoolData(schoolArrayList, context);
                /**Set up and insert Schools into Database using Live and Room so Network calls won't have to be made once we retrieve data**/
            }
        }
    }

    @Override
    public void onListItemClick(School schoolItem) {
        Intent intent = new Intent(this,DetailActivity.class);
        intent.putExtra("school",schoolItem);
        /** Preference to save school */
        startActivity(intent);
    }

    public static School getSchool(int pos){
        return schoolArrayList.get(pos);
        /**Method was going to be used so object can be saved as a preference so network doesn't have to be called*/
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        /**Save Data here so that network calls will not be called each time screen rotates*/
    }
}
