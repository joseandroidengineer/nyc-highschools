package com.jge.a20190221.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.jge.a20190221.Adapters.AdapterViewHolders.SchoolAdapterViewHolder;
import com.jge.a20190221.ListItemClickListener;
import com.jge.a20190221.Models.School;
import com.jge.a20190221.R;

import java.util.ArrayList;

public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapterViewHolder> {
    private ArrayList<School> schools;
    private ListItemClickListener mOnClickListener;
    private Context context;

    public SchoolAdapter(ListItemClickListener mOnClickListener){
        this.mOnClickListener =mOnClickListener;
    }

    @NonNull
    @Override
    public SchoolAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.school_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForListItem, viewGroup, false);
        return new SchoolAdapterViewHolder(view, mOnClickListener, schools);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolAdapterViewHolder schoolAdapterViewHolder, int i) {
        School school = schools.get(i);
        Log.e("SchoolName",school.getSchoolName());
        /*Insert Database here*/
        schoolAdapterViewHolder.schoolNameTextView.setText(school.getSchoolName());
        schoolAdapterViewHolder.boroughTextView.setText(school.getBorough());

    }

    @Override
    public int getItemCount() {
        if(schools == null) return 0;
        return schools.size();
    }

    public void setSchoolData(ArrayList<School> schoolData, Context context){
        schools = schoolData;
        Log.e("setSchools", schools.toString());
        this.context = context;
        notifyDataSetChanged();
    }
}
