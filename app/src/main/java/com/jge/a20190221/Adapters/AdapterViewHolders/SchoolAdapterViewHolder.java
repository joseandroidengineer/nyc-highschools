package com.jge.a20190221.Adapters.AdapterViewHolders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.jge.a20190221.ListItemClickListener;
import com.jge.a20190221.Models.School;
import com.jge.a20190221.R;

import java.util.ArrayList;

public class SchoolAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView schoolNameTextView;
    public TextView boroughTextView;
    private final ListItemClickListener mOnClickListener;
    private ArrayList<School> schools;
    public SchoolAdapterViewHolder(@NonNull View itemView, ListItemClickListener mOnClickListener, ArrayList<School> schools) {
        super(itemView);
        schoolNameTextView = itemView.findViewById(R.id.school_name_tv);
        boroughTextView = itemView.findViewById(R.id.borough_tv);
        this.mOnClickListener = mOnClickListener;
        this.schools = schools;
        Log.e("VIEWHOLDER", schools.get(0).getSchoolName());
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int position = getAdapterPosition();
        mOnClickListener.onListItemClick(schools.get(position));
    }
}