package com.jge.a20190221;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jge.a20190221.Models.SAT;
import com.jge.a20190221.Models.School;
import com.jge.a20190221.Utils.NetworkUtils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;

public class DetailActivity extends AppCompatActivity {

    private School school;
    private TextView satCRTextView;
    private TextView satMTTextView;
    private TextView satWTTextView;
    private TextView overviewTextView;
    private TextView schoolNameTextView;
    private RequestQueue queue;
    private Gson gson;
    private ArrayList<SAT> satArrayList;
    private SAT sat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        school = getIntent().getExtras().getParcelable("school");
        volleyRequest();
        initViews();
        overviewTextView.setText(school.getOverviewParagraph());
        schoolNameTextView.setText(school.getSchoolName());
    }

    private void setViews(){
        satWTTextView.setText(sat.getSatWritingScore());
        satMTTextView.setText(sat.getSatMathScore());
        satCRTextView.setText(sat.getSatReadingScore());
    }

    private void initViews(){
        satCRTextView = findViewById(R.id.SAT_CR_tv);
        satMTTextView = findViewById(R.id.SAT_MT_tv);
        satWTTextView = findViewById(R.id.SAT_WT_tv);
        overviewTextView = findViewById(R.id.overview_tv);
        schoolNameTextView = findViewById(R.id.school_name_tv);
    }



    private void volleyRequest() {
        queue = Volley.newRequestQueue(this);
        String url = String.valueOf(NetworkUtils.buildSATUrl(school.getDbn()));
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                gsonMap(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "There is a volley Error: "+error, Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(jsonArrayRequest);
    }

    private void gsonMap(JSONArray jsonArray){
        if(jsonArray == null){
            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
        }else{
            GsonBuilder gsonBuilder = new GsonBuilder();
            gson = gsonBuilder.create();
            if(jsonArray.length() > 0){
                satArrayList = new ArrayList<>(Arrays.asList(gson.fromJson(jsonArray.toString(), SAT[].class)));
                sat = satArrayList.get(0);
                setViews();
                /**Set up and insert Schools into Database using Live and Room so Network calls won't have to be made once we retrieve data**/
            }else{
                Toast.makeText(this, "SAT data for this school is not available at this time", Toast.LENGTH_LONG).show();
                setErrorViews();
            }

        }

    }

    private void setErrorViews() {
        satCRTextView.setTextColor(Color.RED);
        satMTTextView.setTextColor(Color.RED);
        satWTTextView.setTextColor(Color.RED);
        overviewTextView = findViewById(R.id.overview_tv);
        schoolNameTextView = findViewById(R.id.school_name_tv);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        /**Save Data here so that network calls will not be called each time screen rotates*/
    }
}




